<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Article;

class BlogsController extends Controller
{
    public function index(){
$blogs = Article::all();
       return view('admin.index')
        ->with(compact('blogs'));
    }
    public function edit($id){
    	 $blog=Article::find($id);
        return view('admin.edit')->with(compact('blog'));
    }
    public function destroy($id)
    {
        Article::destroy($id);
        $msgD= "Successfully delete";
        return redirect('admin/blogs')
        ->with ('sms',$msgD);;
    }
    public function update($id, Request $request)
    {
       
      $input= $request->except(['_token','_method']);
     
      if ($request->hasFile('image')) 
      {
         
      $filename =$request->file('image')->getClientOriginalName();
     
      $request->file('image')->move(public_path("uploads"),$filename);
      
      $input['image'] = $filename;
      
      }
       $sms = 'Successfully updated';
      Article::where('id',$id)->update($input);
      return redirect("admin/blogs")
       ->with ('sms',$sms);
    }
}
