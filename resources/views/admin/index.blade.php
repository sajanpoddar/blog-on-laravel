@extends('dashboard.layout.master')

@section('content')
<style>
  .post-preview{
    padding-left: 100px;
    padding-top: 5px;
      }
      .post-subtitle{
        color:#fff;
      }
      .post-meta{
        color:grey;
      }
      .pull-right{
        margin-left: 20px;
      }
</style>
<div class="container">
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1 blog">
            <div class='post-preview'>
                @if(Session::has('sms'))
               <div class="alert alert-success"> {{ Session::get('sms') }}</div>
                @endif
                </div>

               @foreach($blogs as $blog)

                <div class="post-preview">
                    <a href="post.html">
                        <h2 class="post-title">
                            {{$blog->title}}
                        </h2>
                        <div class="pull-right">
                        <a href="admin/blogs/{{$blog->id}}/edit" class="btn btn-primary">Edit</a>
                        <!-- <form  method="get" action="admin/blogs/{{$blog->id}}/edit">
                        <button type="submit">Edit</button>
                        </form> -->
                        <form  method="post" action="admin/blogs/{{$blog->id}}">
                        <input type='hidden' name='_token' value='{{csrf_token()}}'>
                        <input type="hidden" name="_method" value="DELETE">
                        <button type="submit">Delete</button>
                        </form>
                    </div>
                        @if(!empty($blog->image))
                        <img src="/uploads/{{$blog->image}}" alt="" width="500px" height="200px" />
                        @endif
                        <h3 class="post-subtitle">
                            {{$blog->description}}
                        </h3>
                    </a>
                    <p class="post-meta">Posted by <a href="#">{{$blog->created_by}}</a> {{$blog->crated_at}}</p>
                    
                </div>
                @endforeach
                <hr>
               
                
                <!-- Pager -->
                <ul class="pager">
                    <li class="next">
                        <a href="#">Older Posts &rarr;</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
  @stop

 <!--  @section('styles')
    <style type="text/css">
      
    </style>
  @endsection

  @section('scripts')
    <script type="text/javascript">
      
    </script>
  @endsection -->